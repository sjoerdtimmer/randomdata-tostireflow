#include <max6675.h>
#include <LiquidCrystal.h>


// pin constants:

#define NANOPINS

#ifdef NANOPINS
#define BUTTON1 A0
#define BUTTON2 A1
#else
#define BUTTON1 2 // with gnd next to 2 
#define BUTTON2 3 // with gnd next to 2
#endif

#ifdef NANOPINS
#define LCD_RS 7 // A7
#define LCD_E 6 // A6
#define LCD_D4 5 // A5
#define LCD_D5 4 // A4
#define LCD_D6 3 // A3
#define LCD_D7 2 // A2
#else
#define LCD_RS A0
#define LCD_E A1
#define LCD_D4 4
#define LCD_D5 5
#define LCD_D6 6
#define LCD_D7 7
#endif

// same for both
#define BUZZER 9  // with 8 as gnd?

// same for both:
#define MAX6675_SS 10
#define MAX6675_MISO 12
#define MAX6675_SCK 13


#ifdef NANOPINS
#define RELAY 8
#else
#define RELAY A2  // do not forget to think about current limiting: max 20mA per pin!
// potentially use A3 as gnd??
#endif


// stage of reflow:
#define STAGE_ERROR 0 // no temperature reading
#define STAGE_READY 1
#define STAGE_PREHEAT 2
#define STAGE_SOAKING 3
#define STAGE_REFLOW 4
#define STAGE_COOLDOWN 5
#define STRING_ERROR    "  ERROR   " 
#define STRING_READY    "     ready      " 
#define STRING_PREHEAT  " preheat  " 
#define STRING_SOAKING  " soaking  " 
#define STRING_REFLOW   "  reflow  " 
#define STRING_COOLDOWN " cooldown " 
// after reflow it goes back to ready

/* REFLOW profile
 the original profile description is a bit confusing
 the confusing point is that PID is used:
 PID is not appropriate when the target temperature is changing all the time!!
 especially just before the reflow hits it's top the heating has to be switched
 off before the top is reached or it will overshoot. 
 However you do want to heat as long as possible. PID will not do this, 
 but instead start heating more and more slowly towards the top...
 Similarly, when the target temperature is increasing all the time
 PID will heat too slow because it is expecting to end at the current target temp.


 better idea: 
 heuristic for preheat and soak: heat as long as the temperature increase 
 does not exceed the given speed(2-3 and 0.5-1 resp.)
 heuristic for reflow: fully heat until maxT-5 is reached, then stop
 heuristic for cooldown: heater on if the temperature decrease in the last second was more than 5degrees
 

this seems like a good description of the desired profile:
 https://www.rfmw.com/data/MwT_Solder_Flow_recommendations_Sn63%20.pdf
 for all stages minimum and maximum degrees per second are given
		
*/

#define PROFILE_PREHEAT_TEMP 125
#define PROFILE_PREHEAT_RATE 3
#define PROFILE_SOAKING_TEMP 180
#define PROFILE_SOAKING_RATE 1
#define PROFILE_REFLOW_MAX 235
#define PROFILE_REFLOW_EARLYSTOP 30 // used to be 5 but there was a 25 degree overshoot
#define PROFILE_COOLDOWN_TEMP 50
#define PROFILE_COOLDOWN_RATE 5

// app variables:
bool isheatingon = false;
double currenttemp = 0;
double previoustemp = 0;
int jobstarttime = 0;

int stage = STAGE_READY;

// libraries:
LiquidCrystal lcd(LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
MAX6675 thermocouple(MAX6675_SCK, MAX6675_SS, MAX6675_MISO);


void setup() {
  	Serial.begin(9600);
	// configure pins:
	pinMode(RELAY,OUTPUT);
	digitalWrite(RELAY,LOW);

	pinMode(BUTTON1,INPUT_PULLUP);
	pinMode(BUTTON2,INPUT_PULLUP);

	// init lcd:
	lcd.begin(16, 2); // 16x2 display
	Serial.println("time, realtemp, temp, dt, h");
	//lcd.noBlink();

	// wait for MAX chip to stabilize
	delay(500);
}

void updatedisplay(){
	// template:
	// " * 140/230'C    "
 	// " soaking  0:15  "
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print(isheatingon?" * ":" - ");
	print3tempdigits(currenttemp);

	lcd.print("/");
	switch(stage){
		case STAGE_ERROR:    lcd.print("---"); break;
		case STAGE_READY:    lcd.print("---"); break;
		case STAGE_PREHEAT:  print3tempdigits(PROFILE_PREHEAT_TEMP); break;
		case STAGE_SOAKING:  print3tempdigits(PROFILE_SOAKING_TEMP); break;
		case STAGE_REFLOW:   print3tempdigits(PROFILE_REFLOW_MAX); break;
		case STAGE_COOLDOWN: lcd.print("---"); break;
	}
	lcd.write((char)0xDF); // degree symbol
	lcd.print("C");

	// second line
	lcd.setCursor(0, 1);
	switch(stage){
		case STAGE_ERROR:    lcd.print(STRING_ERROR);    break;
		case STAGE_READY:    lcd.print(STRING_READY);    break;
		case STAGE_PREHEAT:  lcd.print(STRING_PREHEAT);  break;
		case STAGE_SOAKING:  lcd.print(STRING_SOAKING);  break;
		case STAGE_REFLOW:   lcd.print(STRING_REFLOW);   break;
		case STAGE_COOLDOWN: lcd.print(STRING_COOLDOWN); break;
	}
	if(stage!=STAGE_READY){ // no runningtime in the READY stage
		int secondsrunning = millis()/1000-jobstarttime;
		int minutes = secondsrunning / 60;
		int seconds = secondsrunning % 60;
		print2timedigits(minutes);
		lcd.print(":");
		print2timedigits(seconds);
	}
}

void heating_on(){
	isheatingon = true;
	digitalWrite(RELAY,HIGH);
}

void heating_off(){
	isheatingon = false;
	digitalWrite(RELAY,LOW);
}

void print3tempdigits(double temp){
	int degrees = (int) temp;
	if(degrees<10)lcd.print(" ");
	if(degrees<100)lcd.print(" ");
	lcd.print(degrees);
}

void print2timedigits(int num){
	if(num<10)lcd.print("0");
	lcd.print(num,DEC);
}



void loop() {
	
	// required data:
	// time
	// temp
	// dt
	// h?
	currenttemp = thermocouple.readCelsius();
	Serial.print(0.001*millis());
	Serial.print(", ");
	Serial.print(currenttemp);
	Serial.print(", ");
	if(stage==STAGE_READY || stage==STAGE_ERROR)
		Serial.print(currenttemp);
	if(stage==STAGE_PREHEAT)
		Serial.print(currenttemp+1000.0);
	if(stage==STAGE_SOAKING)
		Serial.print(currenttemp+2000.0);
	if(stage==STAGE_REFLOW)
		Serial.print(currenttemp+3000.0);
	if(stage==STAGE_COOLDOWN)
		Serial.print(currenttemp+4000.0);
	Serial.print(", ");
	Serial.print(currenttemp-previoustemp);
	Serial.print(", ");
	Serial.println(isheatingon?"1":"0");


	// check the start button
	if(stage==STAGE_READY && digitalRead(BUTTON1)==LOW){
		stage=STAGE_PREHEAT;
		jobstarttime = millis()/1000;
	}// it is a bit ugly that the button is checked only once every second
	// but this also prevents jitter. TODO: find a good button lib and use that

	// check the stop button
	if(digitalRead(BUTTON2)==LOW){
		heating_off();
		stage=STAGE_READY;
	}

	// main algorithm:
	switch(stage){
		case STAGE_PREHEAT:
			// on if not heating too quickly
			if(currenttemp - previoustemp < PROFILE_PREHEAT_RATE){
				heating_on(); }else{ heating_off(); }
			// check if we are done yet
			if(currenttemp > PROFILE_PREHEAT_TEMP){
				stage=STAGE_SOAKING;
			}
			break;
		case STAGE_SOAKING:
			// on if not heating too quickly
			if(currenttemp - previoustemp < PROFILE_SOAKING_RATE){
				heating_on(); }else{ heating_off(); }
			// check if we are done yet
			if(currenttemp > PROFILE_SOAKING_TEMP){
				stage=STAGE_REFLOW;
				heating_on();
			}
			break;
		case STAGE_REFLOW:
			// heating is full on and remains that way
			// check if we are done yet
			if(currenttemp > PROFILE_REFLOW_MAX- PROFILE_REFLOW_EARLYSTOP){
				stage=STAGE_COOLDOWN;
				heating_off();
			}
			break;
		case STAGE_COOLDOWN:
			// on if cooling down too quickly(very unlikely)
			if(previoustemp - currenttemp > PROFILE_COOLDOWN_RATE){
				heating_on(); }else{ heating_off(); }
			// check if we are done yet
			if(currenttemp < PROFILE_COOLDOWN_TEMP){
				stage=STAGE_READY;
				tone(BUZZER,440,3000);
			}
			break;
	}

	updatedisplay();

	previoustemp = currenttemp;
	delay(1000);
}

