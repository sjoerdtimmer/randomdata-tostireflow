#!/bin/bash

PROJECTLIBDIR=$(cd $(dirname "$0") && pwd -P)
SYSTEMLIBDIR=~/sketchbook/libraries

for lib in $PROJECTLIBDIR/*
do
	if [ -d $lib ]; then
		ln -si $lib $SYSTEMLIBDIR/
	fi
done