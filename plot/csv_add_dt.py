#!/bin/python3

import csv
import sys

with open(sys.argv[1], 'r') as csvfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar='|')
	reader.__next__() # discards line 1
	oldtime,_,oldtemp=reader.__next__()
	print("time, dt")
	for row in reader:
		# for _ in range(6):
		# 	reader.__next__() # discards 10 lines
		time,_,temp=reader.__next__()
		dt = (float(temp)-float(oldtemp))/(float(time)-float(oldtime))
		print("%s, %s"%(time,dt))
		oldtime=time
		oldtemp=temp