$fn=15;


//* the SSR:
color([0.9,0.9,0.9]){
	difference(){
		// base
		cube([60,45,22.5],center=true);
		// subtract cavity
		translate([30-7,0,-22.5/2+4])
		hull(){
			cylinder(r=5,h=24);
			translate([10,0,0])
			cylinder(r=5,h=24);
		}
		// hole
		translate([30-2.5-2.25,0,-12])
		hull(){
			cylinder(r=2.25,h=10);
			translate([-3.5,0,0]) // equal space left and right, not from datasheet!
			cylinder(r=2.25,h=10);
		}
		// screw terminals
		translate([30-12+0.1,14-10/2,22.5/2-7+0.1])
		cube([12,10,7]);
		translate([30-12+0.1,-14-10/2,22.5/2-7+0.1])
		cube([12,10,7]);
	}
}
//*/



//* the cover:
thickness=1;
cableroom=4;
terminalheight=7;
/*our thing:
color([0,0.5,1]){
	translate([30-16,-45/2,22.5/2])
	cube([16+thickness+cableroom,45,thickness]);
	translate([30+cableroom,-45/2,-22.5/2])
	cube([thickness,45,22.5]);

	// filling
	difference(){
		translate([30,-45/2,-22.5/2])
		cube([cableroom,45,22.5-terminalheight]);
		translate([30-0.01,-45/2-1,-22.5/2-0.01])
		cube([1,47,2]);
		translate([30-0.01,-11/2,-22.5/2+2-0.02])
		cube([1,11,2]);
	}
	// skewed rim
	translate([30,-45/2,-22.5/2+5])
	multmatrix([
		[1,0,0,0],
		[0,1,0,0],
		[0.2,0,1,0],
		[0,0,0,1]])
	cube([cableroom,45,22.5-terminalheight-5]);
	// ridge
	// translate([30-17,-45/2,22.5/2-0.5])
	// cube([0.5,45,0.5]);

	// plug
	intersection(){
		translate([15,0,4/2])
		cube([30,40,22.5-4],center=true);
		translate([30-7,0,-11.25+3])
		hull(){
		 	cylinder(r=5,h=25);
		 	translate([10,0,0])
		 	cylinder(r=5,h=25);
		}
	}
	// pin
	translate([30-2.5-2.25,0,-22.5/2])
	hull(){
		cylinder(r=2.25,h=10);
		translate([-3.5,0,0])
		cylinder(r=2.25,h=10);
	}
}
//*/

